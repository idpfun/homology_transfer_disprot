args <- commandArgs(trailingOnly = TRUE)
if (length(args) < 4) {
  stop("Supplied two input and output files", call.=FALSE)
} else if (length(args) > 4 ) {
  stop("Supplied only two input and output files", call.=FALSE)
}

load(args[1])
load(args[2])
load(args[3])

datos_ortho_oma <- datos_ortho_oma[datos_ortho_oma$orthologs %in% uniprot_data$Entry, ]
datos_ortho_insp <- datos_ortho_insp[datos_ortho_insp$orthologs %in% uniprot_data$Entry, ]

save(datos_ortho_oma,datos_ortho_insp, file=args[4])
