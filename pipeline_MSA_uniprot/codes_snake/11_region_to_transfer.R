args <- commandArgs(trailingOnly = TRUE)
if (length(args) < 8) {
  stop("Supplied three input, two parameters and two output", call.=FALSE)
} else if (length(args) > 8 ) {
  stop("Supplied only three input, two parameters and two output", call.=FALSE)
}

library("stringr")

normd_full_cutoff <- as.numeric(args[1])
normd_region_cutoff <- as.numeric(args[2])

#path/P04637_clustalO_fullidentity_60.fasta.aln
normd_full <- read.delim(args[3], stringsAsFactors=FALSE)
colnames(normd_full) <- c("ref_uniprot_acc", "quality_full")
normd_full <- normd_full[normd_full$quality_full >= normd_full_cutoff, ]

#remove path depending windows linux "'\'|/"
y <- str_split(normd_full$ref_uniprot_acc, "'\'|/")
normd_full$ref_uniprot_acc <- unlist(lapply(y, FUN=function(X){X[length(X)]}))

y <- str_split(normd_full$ref_uniprot_acc, "_|\\.")
normd_full$ref_uniprot_acc <- unlist(lapply(y, FUN=function(X){X[1]}))
normd_full$method <- unlist(lapply(y, FUN=function(X){X[2]}))
normd_full$full_identity <- unlist(lapply(y, FUN=function(X){X[4]}))

#P04637_clustalO_fullidentity_60_start_14_end_19_regionidentity_60.fasta.aln
normd_region <- read.delim(args[4], stringsAsFactors=FALSE)
normd_region <- normd_region[, c("file","quality")]
colnames(normd_region) <- c("ref_uniprot_acc", "quality_region")
normd_region <- normd_region[normd_region$quality_region >= normd_region_cutoff, ]

#remove path depending windows linux "'\'|/"
y <- str_split(normd_region$ref_uniprot_acc, "'\'|/")
normd_region$ref_uniprot_acc <- unlist(lapply(y, FUN=function(X){X[length(X)]}))

y <- str_split(normd_region$ref_uniprot_acc, "_|\\.")
normd_region$ref_uniprot_acc <- unlist(lapply(y, FUN=function(X){X[1]}))
normd_region$method <- unlist(lapply(y, FUN=function(X){X[2]}))
normd_region$full_identity <- unlist(lapply(y, FUN=function(X){X[4]}))
normd_region$ref_start <- unlist(lapply(y, FUN=function(X){X[6]}))
normd_region$ref_end <- unlist(lapply(y, FUN=function(X){X[8]}))
normd_region$region_identity <- unlist(lapply(y, FUN=function(X){X[10]}))

normd_values <- merge(normd_full, normd_region)
rm(normd_full, normd_region, y)

#mapping_regions
load(args[5])
y <- str_split(mapped_region$file, "_|\\.")
colnames(mapped_region)[5] <- "ref_uniprot_acc"
mapped_region$ref_uniprot_acc <- unlist(lapply(y, FUN=function(X){X[1]}))
mapped_region$method <- unlist(lapply(y, FUN=function(X){X[2]}))
mapped_region$full_identity <- unlist(lapply(y, FUN=function(X){X[4]}))
mapped_start <- mapped_region[mapped_region$pos_type %in% "start", c("uniprot_acc", "map_pos", "ref_pos", "ref_uniprot_acc", "method", "full_identity")]
colnames(mapped_start)[c(2,3)] <- c("start", "ref_start") 
mapped_end <- mapped_region[mapped_region$pos_type %in% "end", c("uniprot_acc", "map_pos", "ref_pos", "ref_uniprot_acc", "method", "full_identity")]
colnames(mapped_end)[c(2,3)] <- c("end", "ref_end") 
rm(mapped_region)

#files in the MSA regions
load(args[6])
y <- str_split(proteins_msa$file, "_|\\.")
colnames(proteins_msa)[1] <- "ref_uniprot_acc"
proteins_msa$ref_uniprot_acc <- unlist(lapply(y, FUN=function(X){X[1]}))
proteins_msa$method <- unlist(lapply(y, FUN=function(X){X[2]}))
proteins_msa$full_identity <- unlist(lapply(y, FUN=function(X){X[4]}))
proteins_msa$ref_start <- unlist(lapply(y, FUN=function(X){X[6]}))
proteins_msa$ref_end <- unlist(lapply(y, FUN=function(X){X[8]}))
proteins_msa$region_identity <- unlist(lapply(y, FUN=function(X){X[10]}))

regions_transfered <- merge(proteins_msa, normd_values)
rm(proteins_msa, normd_values)
regions_transfered <- merge(regions_transfered, mapped_start)
regions_transfered <- merge(regions_transfered, mapped_end)
rm(mapped_start, mapped_end)

#regions term
regions <- read.delim(args[7], stringsAsFactors=FALSE)
colnames(regions)[1:3] <- c("ref_uniprot_acc", "ref_start", "ref_end")
regions_transfered <- merge(regions_transfered, regions)
rm(regions)

regions_transfered <- regions_transfered[, c("ref_uniprot_acc", "ref_start", "ref_end", "method", "full_identity", "region_identity", "quality_full", "quality_region", "uniprot_acc", "start", "end", "term_id")]

regions_transfered <-  regions_transfered[order(regions_transfered$ref_uniprot_acc, regions_transfered$start, regions_transfered$end, regions_transfered$method, regions_transfered$full_identity, regions_transfered$region_identity), ]

write.table(regions_transfered, file=args[8], quote=FALSE, row.names=FALSE)

