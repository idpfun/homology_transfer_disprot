#!/usr/bin/perl

$normd_pro = $ARGV[0]; #folder of normd program
$folder_name = $ARGV[1]; #folders of all data 
$file_output = $ARGV[2]; #file output
open(NORMDTABLE, ">$file_output");
print NORMDTABLE "file\tquality\n";

@MSA_files = glob( $folder_name. '/*' );
#write the quality score
foreach my $n_file (@MSA_files) {
    	chomp($n_file);
    	$v = `$normd_pro $n_file`;
    	chomp($v);			
    	print NORMDTABLE "$n_file\t$v\n";
}
close NORMDTABLE;
exit;
