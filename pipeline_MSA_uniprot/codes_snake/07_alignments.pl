$method_name = $ARGV[0];
$ccc = $ARGV[1];
$out_f = $ARGV[2];
mkdir $out_f;
my @files = glob( $ccc . '/*' );
foreach my $file_fasta (@files){    
	chomp $file_fasta;
	$out_file = $file_fasta =~ s/\.fasta//r;
	$out_file = $out_file =~ s/$ccc/$out_f/r;
	$kkk = "";
	if ($method_name eq "clustalo"){
        	$kkk = "clustalo -i ". $file_fasta. " -o ". $out_file. "_clustalO.fasta.aln -t Protein --infmt fasta --outfmt fasta --output-order input";
    }elsif($method_name eq "mafft"){
        $kkk = "mafft --quiet ". $file_fasta. " > ". $out_file. "_mafft.fasta.aln";
    }	
	if($kkk ne ""){
		print $kkk;
        	`$kkk`;  
	} 
}

