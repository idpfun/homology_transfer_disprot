#!/bin/bash 
./../cd-hit/cd-hit -i ../data/03_disprot_uniprot_proteins.fasta -o ../data/04_cdhit_output/04_cdhit_protein_040 -c 0.40 -n 2
./../cd-hit/cd-hit -i ../data/03_disprot_uniprot_proteins.fasta -o ../data/04_cdhit_output/04_cdhit_protein_045 -c 0.45 -n 2
./../cd-hit/cd-hit -i ../data/03_disprot_uniprot_proteins.fasta -o ../data/04_cdhit_output/04_cdhit_protein_050 -c 0.50 -n 3
./../cd-hit/cd-hit -i ../data/03_disprot_uniprot_proteins.fasta -o ../data/04_cdhit_output/04_cdhit_protein_055 -c 0.55 -n 3
./../cd-hit/cd-hit -i ../data/03_disprot_uniprot_proteins.fasta -o ../data/04_cdhit_output/04_cdhit_protein_060 -c 0.60 -n 4
./../cd-hit/cd-hit -i ../data/03_disprot_uniprot_proteins.fasta -o ../data/04_cdhit_output/04_cdhit_protein_065 -c 0.65 -n 4
./../cd-hit/cd-hit -i ../data/03_disprot_uniprot_proteins.fasta -o ../data/04_cdhit_output/04_cdhit_protein_070 -c 0.70 -n 5
./../cd-hit/cd-hit -i ../data/03_disprot_uniprot_proteins.fasta -o ../data/04_cdhit_output/04_cdhit_protein_075 -c 0.75 -n 5
./../cd-hit/cd-hit -i ../data/03_disprot_uniprot_proteins.fasta -o ../data/04_cdhit_output/04_cdhit_protein_080 -c 0.80 -n 5
./../cd-hit/cd-hit -i ../data/03_disprot_uniprot_proteins.fasta -o ../data/04_cdhit_output/04_cdhit_protein_085 -c 0.85 -n 5
./../cd-hit/cd-hit -i ../data/03_disprot_uniprot_proteins.fasta -o ../data/04_cdhit_output/04_cdhit_protein_090 -c 0.90 -n 5
./../cd-hit/cd-hit -i ../data/03_disprot_uniprot_proteins.fasta -o ../data/04_cdhit_output/04_cdhit_protein_095 -c 0.95 -n 5
./../cd-hit/cd-hit -i ../data/03_disprot_uniprot_proteins.fasta -o ../data/04_cdhit_output/04_cdhit_protein_100 -c 1.00 -n 5

