#read all files processed with limpiar_cdhit.pl
x <- dir("data/05_cdhit_cleaned", pattern="cdhit_table", full.names = TRUE)
cdhit_table <- data.frame()
for (i in x){
	y <- read.delim(file=i, sep="\t", stringsAsFactors=FALSE)
	cdhit_table <- rbind(cdhit_table, y)
}

save(cdhit_table, file="data/05_output_cdhit.Rdata")
