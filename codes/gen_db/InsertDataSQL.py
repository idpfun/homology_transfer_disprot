import mysql.connector
from mysql.connector import Error
from dynaconf import settings
from CreateSQL import create_db

def upload_db():
    protein__protein_accession = read_data(file_protein)
    msa_all__file_name_MA_accession = read_data(file_msa_all)
    msa_region__file_name_MR_accession = read_data(file_msa_region)
    P_has_MF__protein_accession__file_name_MA_accession = read_data(file_P_has_MF)
    P_has_MR__protein_accession__file_name_MR_accession = read_data(file_P_has_MR)
    term__term_id = read_data_term(file_term)
    Term_has_MR__file_name_MR_accession_term_id = read_data(file_Term_has_MR)

    ###########
    # protein #
    ###########
    sql_protein__protein_accession = "INSERT INTO protein (protein_accession, class_type, status_up, taxon_id, organism, disprot_id, length) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    insert(sql_protein__protein_accession, protein__protein_accession)

    ###########
    # msa_all #
    ###########
    sql_msa_all__file_name_MA_accession = "INSERT INTO msa_full (msafull_id, file_name_msafull, protein_accession_ref, disprot_perc_id, aln_method, ortho_perc_id, normd_score_full) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    insert(sql_msa_all__file_name_MA_accession, msa_all__file_name_MA_accession)

    ##############
    # msa_region #
    ##############
    sql_msa_region__file_name_MR_accession = "INSERT INTO msa_region (msaregion_id, file_name_msaregion, msafull_id, region_start, region_end, ortho_region_perc_id, normd_score_region) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    insert(sql_msa_region__file_name_MR_accession, msa_region__file_name_MR_accession)

    ############
    # P_has_MF #
    ############
    sql_P_has_MF__protein_accession__file_name_MA_accession = "INSERT INTO P_has_MF (protein_accession, msafull_id, ortho_perc_exact_id) VALUES (%s, %s, %s)"
    insert(sql_P_has_MF__protein_accession__file_name_MA_accession, P_has_MF__protein_accession__file_name_MA_accession)

    ############
    # P_has_MR #
    ############
    sql_P_has_MR__protein_accession__file_name_MR_accession = "INSERT INTO P_has_MR (protein_accession, msaregion_id, ortho_region_exact_perc_id) VALUES (%s, %s, %s)"
    insert(sql_P_has_MR__protein_accession__file_name_MR_accession, P_has_MR__protein_accession__file_name_MR_accession)

    ########
    # term #
    ########

    sql_term__term_id = "INSERT INTO term (term_id, ontology_name, term_name) VALUES (%s, %s, %s)"
    insert(sql_term__term_id, term__term_id)

    ###############
    # Term_has_MR #
    ###############
    sql_Term_has_MR__file_name_MR_accession_term_id = "INSERT INTO Term_has_MR (msaregion_id, term_id) VALUES (%s, %s)"
    insert(sql_Term_has_MR__file_name_MR_accession_term_id, Term_has_MR__file_name_MR_accession_term_id)


def insert(command, values):
    for val in values:
        try:
            cursor.executemany(command, val)
            cnx.commit()
            print(cursor.rowcount, "was inserted.")
        except mysql.connector.IntegrityError:
            print("already inserted")

def read_data(file):
    data = []
    d = []
    row_numb = 0
    with open(file) as fh_input:
        for ind, line in enumerate(fh_input):
            line = line.strip("\n")
            if row_numb == insert_row_limit:
                d.append(data)
                data = []
                row_numb = 0
            data.append(tuple(line.split(',')))
            row_numb += 1
    d.append(data)
    return d

def read_data_term(file):
    data = []
    d = []
    row_numb = 0
    with open(file) as fh_input:
        for line in fh_input:
            line = line.strip("\n")
            if line.count(",") == 3:
                line = line.replace(", ", "-")
            if row_numb == insert_row_limit:
                d.append(data)
                data = []
                row_numb = 0
            data.append(tuple(line.split(',')))
            row_numb += 1
    d.append(data)
    return d


########################################################################################################################

insert_row_limit = settings["INSERTED_ROW_LIMIT"]


root = "/home/matyi/Projects/Homology_transfer"

root_out = "{0}/data_for_insert_test3".format(root)
#root_out = "{0}/data_for_insert".format(root)
file_msa_all = "{0}/msa_full.txt".format(root_out)
file_protein = "{0}/protein.txt".format(root_out)
file_P_has_MF = "{0}/P_has_MF.txt".format(root_out)
file_P_has_MR = "{0}/P_has_MR.txt".format(root_out)
file_msa_region = "{0}/msa_region.txt".format(root_out)
file_term = "{0}/term.txt".format(root_out)
file_Term_has_MR = "{0}/Term_has_MR.txt".format(root_out)


create_db()
#exit()
try:
    cnx = mysql.connector.connect(
        host=settings["HOST"],
        user=settings["USERNAME"],
        password=settings["PASSWORD"],
        database=settings["DATABASE"],
        auth_plugin='mysql_native_password'
    )
    cursor = cnx.cursor()
    if cnx.is_connected():
        db_Info = cnx.get_server_info()
        try:
            cursor.execute("select database();")
            record = cursor.fetchone()
            print("You're connected to database: ", str(record[0]))
            upload_db()
            print("Done")
            cnx.close()
        except Error as e:
            print("Error while connecting to MySQL", e)
            exit()
    else:
        print("Connection failed")
        exit()
except Error as e:
    print("Error while connecting to MySQL", e)
    exit()

