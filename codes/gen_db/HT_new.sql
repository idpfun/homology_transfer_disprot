CREATE TABLE `protein` (
  `protein_accession` varchar(10) PRIMARY KEY,
  `class_type` varchar(20) NOT NULL,
  `status_up` varchar(10) NOT NULL,
  `taxon_id` int(20),
  `organism` varchar(256),
  `disprot_id` varchar(20),
  `length` int(6) NOT NULL
);
CREATE TABLE `msa_full` (
  `msafull_id` mediumint(10) PRIMARY KEY,
  `file_name_msafull` varchar(256) UNIQUE NOT NULL,
  `protein_accession_ref` varchar(10),
  `disprot_perc_id` float(4,3) NOT NULL,
  `aln_method` varchar(10) NOT NULL,
  `ortho_perc_id` float(4,3) NOT NULL,
  `normd_score_full` float(4,2) NOT NULL
);
CREATE TABLE `msa_region` (
  `msaregion_id` mediumint(10) PRIMARY KEY,
  `file_name_msaregion` varchar(256) UNIQUE NOT NULL,
  `msafull_id` mediumint(10) NOT NULL,
  `region_start` smallint NOT NULL,
  `region_end` smallint NOT NULL,
  `ortho_region_perc_id` float(4,3) NOT NULL,
  `normd_score_region` float(4,2) NOT NULL
);
CREATE TABLE `P_has_MF` (
  `protein_accession` varchar(10),
  `msafull_id` mediumint(10),  
  `ortho_perc_exact_id` float(4,3) NOT NULL,
  PRIMARY KEY (`protein_accession`, `msafull_id`)
);
CREATE TABLE `P_has_MR` (
  `protein_accession` varchar(10),
  `msaregion_id` mediumint(10),
  `ortho_region_exact_perc_id` float(4,3) NOT NULL,
  PRIMARY KEY (`protein_accession`, `msaregion_id`)
);
CREATE TABLE `term` (
  `term_id` varchar(20) PRIMARY KEY,
  `ontology_name` varchar(100) NOT NULL,
  `term_name` varchar(500) NOT NULL
);
CREATE TABLE `Term_has_MR` (
  `msaregion_id` mediumint(10),
  `term_id` varchar(20),
  PRIMARY KEY (`term_id`, `msaregion_id`)
);
ALTER TABLE `msa_full` ADD FOREIGN KEY (`protein_accession_ref`) REFERENCES `protein` (`protein_accession`);
ALTER TABLE `msa_region` ADD FOREIGN KEY (`msafull_id`) REFERENCES `msa_full` (`msafull_id`);
ALTER TABLE `P_has_MF` ADD FOREIGN KEY (`protein_accession`) REFERENCES `protein` (`protein_accession`);
ALTER TABLE `P_has_MF` ADD FOREIGN KEY (`msafull_id`) REFERENCES `msa_full` (`msafull_id`);
ALTER TABLE `P_has_MR` ADD FOREIGN KEY (`protein_accession`) REFERENCES `protein` (`protein_accession`);
ALTER TABLE `P_has_MR` ADD FOREIGN KEY (`msaregion_id`) REFERENCES `msa_region` (`msaregion_id`);
ALTER TABLE `Term_has_MR` ADD FOREIGN KEY (`msaregion_id`) REFERENCES `msa_region` (`msaregion_id`);
ALTER TABLE `Term_has_MR` ADD FOREIGN KEY (`term_id`) REFERENCES `term` (`term_id`);