import mysql.connector
from mysql.connector import Error
from dynaconf import settings

def create_db():
    try:
        cnx = mysql.connector.connect(
            host=settings["HOST"],
            user=settings["USERNAME"],
            password=settings["PASSWORD"],
            auth_plugin='mysql_native_password'
        )
        cursor = cnx.cursor()
        if cnx.is_connected():
            db_Info = cnx.get_server_info()
            print("Connected to MySQL Server version ", db_Info)
            cursor.execute("DROP DATABASE IF EXISTS {0}".format(settings["DATABASE"]))
            cursor.execute("CREATE DATABASE IF NOT EXISTS {0} DEFAULT CHARACTER SET utf8".format(settings["DATABASE"]))
            try:
                cursor.execute("USE {0}".format(settings["DATABASE"]))
                cursor.execute("select database();")
                record = cursor.fetchone()
                print("Database", str(record[0]), "was created successfully")
                file = open(settings["DATABASE_SCHEMA"])
                sql_schema = file.read().replace("\n"," ")
                file.close()
                s = filter(None, sql_schema.split(';'))
                for i in s:
                    cursor.execute(i.strip() + ';')
                print("Database schema is done")
                cnx.close()
            except Error as e:
                print("Error while connecting to MySQL", e)
                exit()
        else:
            print("Connection failed")
            exit()
    except Error as e:
        print("Error while connecting to MySQL", e)
        exit()
