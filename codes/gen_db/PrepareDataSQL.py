def normD(score_file):
    with open(score_file) as fh_scrfile:
        for line in fh_scrfile:
            if line.startswith("file_name"):
                continue
            normD_scores[line.split()[0]] = round(float(line.split()[1].strip("\n")), 2)

def identity(ident_file):
    with open(ident_file) as fh_identfile:
        for line in fh_identfile:
            if line.startswith("sequence_name"):
                continue
            IdentityValues[line.split()[0].split("_")[0] + "-" + line.split()[1]] = round(float(line.split()[2].strip("\n")), 2)

def get_terms(onto_file):
    with open(onto_file) as fh_ontofile:
        for line in fh_ontofile:
            if line.startswith("disprot_id"):
                continue
            if not line.split()[4] in terms:
                terms[line.split()[4]] = line.split("\t")[5].strip("\n")
            if not line.split()[1] in proteins_terms:
                proteins_terms[line.split()[1]] = {}
            region = line.split()[2] + "-" + line.split()[3]
            if not region in proteins_terms[line.split()[1]]:
                proteins_terms[line.split()[1]][region] = []
            if not line.split()[4] in proteins_terms[line.split()[1]][region]:
                proteins_terms[line.split()[1]][region].append(line.split()[4])

def get_prot_data(file):
    with open(file) as fh_pd:
        for line in fh_pd:
            if line.startswith("uniprot_acc"):
                continue
            line = line.strip("\n")
            ProtData[line.split()[0]] = line.split("\t")[1:]

def write_file(file, data):
    with open(file, 'w') as fh_wf:
        for d in data:
            fh_wf.write(','.join(str(x) for x in list(d)) + "\n")


root = "/home/matyi/Projects/Homology_transfer"

root_input = "{0}/18_data_for_query".format(root)
#file_prot_msa = "{0}/18_prot_msa-TEST2.txt".format(root_input)
file_prot_msa = "{0}/18_prot_msa.txt".format(root_input)
file_normD_full_msa = "{0}/18_normD_full_msa.txt".format(root_input)
file_normD_region_msa = "{0}/18_normD_region_msa.txt".format(root_input)
file_msa_full_identity = "{0}/18_msa_full_identity.txt".format(root_input)
file_msa_region_identity = "{0}/18_msa_region_identity.txt".format(root_input)
file_regions_go = "{0}/18_regions_go.txt".format(root_input)
file_regions_idpo = "{0}/18_regions_idpo.txt".format(root_input)
file_prot_data = "{0}/18_prot_data.txt".format(root_input)

root_out = "{0}/data_for_insert_test3".format(root)
# root_out = "{0}/data_for_insert".format(root)
file_msa_all = "{0}/msa_full.txt".format(root_out)
file_protein = "{0}/protein.txt".format(root_out)
file_P_has_MF = "{0}/P_has_MF.txt".format(root_out)
file_P_has_MR = "{0}/P_has_MR.txt".format(root_out)
file_msa_region = "{0}/msa_region.txt".format(root_out)
file_term = "{0}/term.txt".format(root_out)
file_Term_has_MR = "{0}/Term_has_MR.txt".format(root_out)

# Collect Terms #
terms = {}
proteins_terms = {}
get_terms(file_regions_go)
get_terms(file_regions_idpo)
#print(proteins_terms["P35637"])


# Collect NormD scores #
normD_scores = {}
normD(file_normD_full_msa)
normD(file_normD_region_msa)

# Collect Identity values
IdentityValues = {}
identity(file_msa_full_identity)
identity(file_msa_region_identity)


# Collect protein data #
ProtData = {}
get_prot_data(file_prot_data)

protein__accession = [] #1: protein > accession, class
msa_full__msafull_id = [] #2: msa_all > file_name_MA, accession_ref, nd_scr
P_has_MF__accession__file_name_MA = [] #3: P_has_MF > accession, file_name_MA, disprot_perc_id, ortho_perc_exact_id, aln_method
P_has_MR__accession__file_name_MR = [] #4: P_has_MR > accession, file_name_MR, ortho_region_exact_perc_id
msa_full__msa_region = {}
msa_region__ontology = {}
number_file_name_MA = 0
number_file_name_MR = 0
number_file_name_MA_filename = {}
number_file_name_MR_filename = {}
with open(file_prot_msa) as fh_prot_msa:
    for ind, line in enumerate(fh_prot_msa):
        line = line.strip("\n")
        if line.startswith("sequence_name"):
            continue

        # if line != "P35637_disprot_1ref	clustalO_identity_80_cluster_723_seqidentity_60_start_1_end_163_region_go-idpo_identity_60.fasta.aln	data/12_regions/clustalO/seqident60	P35637	disprot reference":
        #     continue
        # print(line)
        if ind % 1000 == 0:
            print("\rline", ind,"     done", end='')
        ###########
        # protein #
        ###########
        accession = (
                     line.split()[0].split("_")[0], #accession
                     line.split()[0].split("_")[1], #ortholog or disprot
                     ProtData[line.split()[0].split("_")[0]][3], #Uniprot calss: sp/tr
                     ProtData[line.split()[0].split("_")[0]][1], #taxon id
                     ProtData[line.split()[0].split("_")[0]][0], #organism
                     ProtData[line.split()[0].split("_")[0]][4], #disprot accession
                     ProtData[line.split()[0].split("_")[0]][2], #protein length
                     )
        if accession not in protein__accession:
            protein__accession.append(accession)
        # exit()
        ###########
        # msa_full #
        ###########
        if "1ref" in line.split()[0]:
            IdentityValues[line.split()[0].split("_")[0] + "-" + line.split()[1]] = float(1)
            if not "start" in line.split()[1]:
                number_file_name_MA_filename[line.split()[1]] = number_file_name_MA
                msa_full__msafull_id.append(
                                                (number_file_name_MA,
                                                    line.split()[1],
                                                    line.split()[0].split("_")[0],
                                                    int(line.split()[1].split("_")[2])/100,
                                                    line.split()[1].split("_")[0],
                                                    int(line.split()[1].split("_")[6].split(".")[0])/100,
                                                    normD_scores[line.split()[1]]
                                                )
                                            )
                number_file_name_MA += 1
            else:
                if not "_".join(line.split()[1].split("_")[:7]) + ".fasta.aln" in msa_full__msa_region:
                    msa_full__msa_region["_".join(line.split()[1].split("_")[:7]) + ".fasta.aln"] = []
                    msa_full__msa_region["_".join(line.split()[1].split("_")[:7]) + ".fasta.aln"].append(line.split()[1])
                elif not line.split()[1] in msa_full__msa_region["_".join(line.split()[1].split("_")[:7]) + ".fasta.aln"]:
                    msa_full__msa_region["_".join(line.split()[1].split("_")[:7]) + ".fasta.aln"].append(line.split()[1])
                number_file_name_MR_filename[line.split()[1]] = number_file_name_MR
                number_file_name_MR += 1

        ############
        # P_has_MF #
        ############
        exact_perc_ident = IdentityValues[line.split()[0].split("_")[0] + "-" + line.split()[1]]  # !#
        if not "start" in line.split()[1]:
            P_has_MF__accession__file_name_MA.append(
                                                        (
                                                          line.split()[0].split("_")[0],
                                                          number_file_name_MA_filename[line.split()[1]],
                                                          exact_perc_ident
                                                        )
                                                     )
        ############
        # P_has_MR #
        ############
        else:
            P_has_MR__accession__file_name_MR.append(
                                                        (
                                                        line.split()[0].split("_")[0],
                                                        number_file_name_MR_filename[line.split()[1]],
                                                        exact_perc_ident
                                                        )
                                                    )
            if not line.split()[1] in msa_region__ontology:
                region = line.split("start_")[1].split("_")[0] + "-" + line.split("end_")[1].split("_")[0]
                msa_region__ontology[line.split()[1]] = proteins_terms[line.split()[0].split("_")[0]][region]

##############
# msa_region #
##############
msa_region__file_name_MR = [] # msa_region > file_name_MR, file_name_MA, region_start, region_end, ortho_region_perc_id, go_name
for full_aln, region_alns in msa_full__msa_region.items():
    if len(region_alns) != 0:
        for region_aln in region_alns:
            s = region_aln.split("start_")[1].split("_")[0]
            e = region_aln.split("end_")[1].split("_")[0]
            msa_region__file_name_MR.append(
                                                (
                                                number_file_name_MR_filename[region_aln],
                                                region_aln,
                                                number_file_name_MA_filename[full_aln],
                                                int(s),
                                                int(e),
                                                int(region_aln.split("_")[-1].split(".")[0])/100,
                                                normD_scores[region_aln]
                                                )
                                            )
print("\n")

########
# term #
########
term__term_id = [] # term > ontology_name, term_id, term_name
for term_id, describ in terms.items():
    term__term_id.append((term_id, term_id.split(":")[0], describ))


###############
# Term_has_MR #
###############
Term_has_MR__file_name_MR_term_id = [] # Term_has_MR > file_name_MR, term_id
for region_aln, ontologies in msa_region__ontology.items():
    # if region_aln != "mafftD_identity_80_cluster_723_seqidentity_80_start_1_end_163_region_go-idpo_identity_60.fasta.aln":
    #     continue
    #print(region_aln, ontologies)
    for ontology in ontologies:
        Term_has_MR__file_name_MR_term_id.append(
                                                    (
                                                    number_file_name_MR_filename[region_aln],
                                                    ontology
                                                    )
                                                )


write_file(file_msa_all, msa_full__msafull_id)
write_file(file_protein, protein__accession)
write_file(file_P_has_MF, P_has_MF__accession__file_name_MA)
write_file(file_P_has_MR, P_has_MR__accession__file_name_MR)
write_file(file_msa_region, msa_region__file_name_MR)
write_file(file_term, term__term_id)
write_file(file_Term_has_MR, Term_has_MR__file_name_MR_term_id)

