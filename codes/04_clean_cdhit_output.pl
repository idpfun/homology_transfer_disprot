#!/usr/bin/perl
#files with the output of cd-hit
@cortes = ("040", "045", "050", "055", "060", "065", "070", "075", "080", "085", "090", "095", "100");
foreach my $n (@cortes) {
open(CDHITFILE, "../data/04_cdhit_output/04_cdhit_protein_$n.clstr");
#fichero para escribir los datos arreglados
open(CDHITTABLE, ">../data/05_cdhit_cleaned/05_cdhit_table_$n.txt");
print CDHITTABLE "cut_off\tcluster\tuniprot\tpercent\n";
$c = 0;
while( $linea = <CDHITFILE> ) {
	chomp $linea;
	if ($linea =~ /^>Cluster\s+(\d+)$/){
		$c = $1;
	}else{
		$linea =~ /^\d+\s+\d+aa,\s+>([a-zA-Z0-9]+)(\.+\s+a?t?\s*)(\*|[0-9\.]*)(.*)$/;
		print CDHITTABLE "$n\t$c\t$1\t$3\n";
	}
}

close CDHITFILE;
close CDHITTABLE;
}
exit;
