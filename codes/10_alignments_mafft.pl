@mafft_names = ("mafftD", "mafftL", "mafftG", "mafftE");
@mafft_command = ("mafft", "linsi", "ginsi", "einsi");
@n = (0..3);
`mkdir data/10_align`;
foreach my $mafft_type (@n){	
	`mkdir data/10_align/$mafft_names[$mafft_type]`;
}
$ccc = "data/09_cluster_fastas";
my @files = `ls $ccc`;
foreach my $file_fasta (@files){    
	chomp $file_fasta;
	$in_file = $ccc."/".$file_fasta;   
	foreach my $mafft_type (@n){	
		$out_file = "data/10_align/". $mafft_names[$mafft_type]. "/". $mafft_names[$mafft_type]. "_". $file_fasta. ".aln";
		$kkk = $mafft_command[$mafft_type]. " ". $in_file. " > ". $out_file;
		print $kkk;
		`$kkk`;
	}        
}

