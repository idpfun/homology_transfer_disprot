#!/usr/bin/perl

$normd_pro = $ARGV[0]; #folder of normd program
$folder_name = $ARGV[1]; #folder of all data 
$file_output = $ARGV[2]; #file output
$folder_select = $ARGV[4]; #seqidentall, seqident30, seqident60, seqident80 
$algorith_name = $ARGV[3]; #clustalO or mafftD
open(NORMDTABLE, ">$file_output");
print NORMDTABLE "algorithm\tselection\tfile_cluster\tcluster_cut_off\tcluster_number\tquality\n";

my $ffff = $folder_name."/".$algorith_name."/".$folder_select;
@corte_files = `ls $ffff`;
#write the quality score

foreach my $n_file (@corte_files) {
	chomp($n_file);
	@f_name = split("\\.|_", $n_file);
	$cluster_n = @f_name[4];
	$cut_off = @f_name[2];
	$v = `$normd_pro/normd $ffff/$n_file`;
	chomp($v);			
	print NORMDTABLE "$algorith_name\t$folder_select\t$n_file\t$cut_off\t$cluster_n\t$v\n";
}
close NORMDTABLE;
exit;
