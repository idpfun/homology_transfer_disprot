Git global setup
git config --global user.name "Elizabeth Martinez Perez"
git config --global user.email "emartinezperez1990@gmail.com"

Create a new repository
git clone git@gitlab.com:idpfun/homology_transfer_disprot.git
cd ped_back-end_code
touch README.md
git add README.md
git commit -m "add README" -a
git push -u HOMOLOGY master

Push an existing folder
cd existing_folder
git init
git remote add HOMOLOGY git@gitlab.com:idpfun/homology_transfer_disprot.git
git add .
git commit -m "commit" -a
git push -u HOMOLOGY master

Push an existing Git repository
cd existing_repo
git remote rename HOMOLOGY old-HOMOLOGY
git remote add HOMOLOGY git@gitlab.com:idpfun/homology_transfer_disprot.git
git push -u HOMOLOGY --all

Pull
git pull HOMOLOGY master

